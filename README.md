# ePaper (Waveshare 2.7 inch) Stock display

## Requires 
https://github.com/waveshare/e-Paper

linked:
 - RaspberryPi_JetsonNano/python/lib
 - RaspberryPi_JetsonNano/python/pic 

stock_config.yaml, create one from stock_config_example.yaml

python3

python3 libs
 - gpiozero
 - spidev
 - PyYAML
 - RPi.GPIO
 - yahoofinancials

## Features
epaper_stock.py
 - Display Stocks
 - Highly configurable (YAML)
 - Rotates over configured stocks
   - Featuring individual configuration
   - Rotation time configurable
     - General
     - By stock
 - Displayed info
   - Name
   - Current Time
   - Market state
   - Current price
     - Optionally sum of your amount of shares
   - Change value, percentage
     - Optionally sum of your amount of shares
     - Optionally based on your buy-in
   - Displays powered-off picture of latest data
     (Featuring unplugged plug picture so you know it's off)

## Installation
 - Enable SPI with raspi-config
 - Clone https://github.com/waveshare/e-Paper @ pi's home
 - Clone this repo @ pi's home
 - Link lib & pic to this projects directory
   - `ln -s /home/pi/e-Paper/RaspberryPi_JetsonNano/python/lib /home/pi/epaper/`
   - `ln -s /home/pi/e-Paper/RaspberryPi_JetsonNano/python/pic /home/pi/epaper/`
 - Copy the plugoff_50px.png into the pic directory
   - `cd /home/pi/epaper/; cp plugoff_50px.png pic`
 - Run installer optionally

## Usage
 - Run with python3 single or as service. Requires root for powerdown.
 - Buttons:
   - Top: Rotate to next stock immediately (as fast as epaper can)
   - Bottom: Press twice for poweroff display and power down
     All other buttons will cancel out the first press
