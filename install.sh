#!/bin/bash
# Minimal installer to be run on Raspberry Pi (Zero)

echo "This will set up the epaper service and default config file."
echo "Nothing will be overwritten if it already exists."
read -p "Do you want to proceed? " -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
	if ! [ -f /lib/systemd/system/epaper.service ]; then
		sudo cp systemd/epaper.service /lib/systemd/system/epaper.service
		sudo systemctl daemon-reload
		sudo systemctl enable epaper.service
        else
		echo "epaper.service already exists."
	fi
	
	if ! [ -f stock_config.yaml ]; then
		cp stock_config_example.yaml stock_config.yaml
	else
		echo "stock_config.yaml already exists."
	fi
	echo "Please change 'stock_config.yaml' and run with 'sudo systemctl start epaper'."
fi

