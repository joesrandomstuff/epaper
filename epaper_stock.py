#!env /bin/python3
# -*- coding:utf-8 -*-
import sys
import os
import multiprocessing
from queue import Empty
from yahoofinancials import YahooFinancials
from datetime import datetime
from datetime import timedelta
import logging

picdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pic")
libdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "lib")
conffile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "stock_config.yaml")

if os.path.exists(libdir):
    sys.path.append(libdir)

from waveshare_epd import epd2in7
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import yaml
from gpiozero import Button
from signal import pause

logging.basicConfig(level=logging.INFO)

# Fonts
font14 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 14)
font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
font65 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 65)

# Open config file
try:
  with open(conffile, 'r') as file:
    stock_config = yaml.full_load(file)
except FileNotFoundError:
  print("Configuration file stock_config.yaml missing.")
  print("Please use stock_config_example.yaml as a template.")
  sys.exit()

# Get configuration
logging.debug("Reading configuration")

# Element positions
for element in stock_config.get("config").get("element_pos"):
  if element[-4:] == 'font':
    globals()[element] = getattr(sys.modules[__name__], stock_config.get("config").get("element_pos").get(element))
  else:
    globals()[element] = stock_config.get("config").get("element_pos").get(element)

startTime = time.time()
stocks = stock_config.get("stock")
sleeptime = stock_config.get("config").get("refresh")
hrs_delta = stock_config.get("config").get("hrs_delta")
auto_off = stock_config.get("config").get("auto_off")
auto_off_marketstate = stock_config.get("config").get("auto_off_marketstate")

logging.debug("Init and clear display")
epd = epd2in7.EPD()
epd.init()
epd.Clear(0xFF)

def elapsedTime():
    # Have 10 Minutes elapsed since the start of the script?
    if (time.time() - startTime) >= 600:
      return True
    else:
      return False

def draw_stock(stockname, sleeptime, hrs_delta, q):
  try:

      T = datetime.now().strftime("%d.%m.%Y %H:%M")
      stockname_old = stockname

      # Check manual shutdown
      poweroff = getbuttonstatus(q, 3)
      try:
        if poweroff == True:
          stockname = list(stock_config.get("stock"))[0]
          logging.info("Stock information display override for manual poweroff: " + stockname)
          pass
      except NameError:
        poweroff = False

      # Check auto shutdown
      S = YahooFinancials(stockname).get_stock_price_data()[stockname]
      try:
        if auto_off and ( S['marketState'] in auto_off_marketstate ) and elapsedTime() == True:
          stockname = list(stock_config.get("stock"))[0]
          logging.info("Stock information display override for auto poweroff: " + stockname)
          poweroff = True
      except NameError:
        pass

      if stockname_old != stockname:
        S = YahooFinancials(stockname).get_stock_price_data()[stockname]

      if poweroff == False:
        logging.info("Stock information display " + stockname)
      logging.debug("draw_stock poweroff: " + str(poweroff))

      if S['marketState'] == 'PRE':
        market = 'pre'
      elif S['marketState'] == 'PREPRE':
        market = 'regular'
      elif S['marketState'] == 'REGULAR':
        market = 'regular'
      else:
        market = 'post'
  
      Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
      draw = ImageDraw.Draw(Himage)

      # Stock name
      draw.text((namepos_x, namepos_y), stockname, font = namefont, fill = 0)
      draw.text((longnamepos_x, longnamepos_y), S['longName'], font = longnamefont, fill = 0)

      # Current time at which display was refreshed
      dcontent = str(T)
      wd, hd = draw.textsize(dcontent, font=localtimefont)
      draw.text((align_right_offset - wd, localtimepos_y), dcontent, font = localtimefont, fill = 0)

      # PRE/POST Market display
      if not S['marketState'] == 'REGULAR':
        wm, hm = draw.textsize(S['marketState'], font=marketstatefont)
        draw.text((align_right_offset - wm, marketstatepos_y), S['marketState'], font = marketstatefont, fill = 0)

      # Power off note
      if poweroff == True:
        bmp = Image.open(os.path.join(picdir, poweroffimg))
        Himage.paste(bmp, (poweroffpos_x, poweroffpos_y))

      # Stock value
      # If amount is configured for the stock, consider this in value
      try:
        multiplier = stock_config.get("stock")[stockname]["amount"]
      except (KeyError, TypeError):
        multiplier = 1
      # Assign sigma as multiplication QTY indicator
      if multiplier > 1:
        sigma = 'Σ'
      else:
        sigma = ''
      scontent = S['currencySymbol'] + str(round(S[market + 'MarketPrice'] * multiplier)) + sigma
      ws, hs = draw.textsize(scontent, font=valuefont)
      draw.text((align_right_offset - ws, valuepos_y), scontent, font = valuefont, fill = 0)

      # Stock change
      try:
        change = S[market + 'MarketPrice'] - stock_config.get("stock")[stockname]["buyin"]
        changepc = (change * 100 / S[market + 'MarketPrice']) / 100
        portfolio = 'P'
      except (KeyError, TypeError):
        change = S[market + 'MarketChange']
        changepc = S[market + 'MarketChangePercent']
        portfolio = ''

      if change >= 0:
        changeicon = '▲'
      else:
        changeicon = '▼'
      # If amount is configured for the stock, consider this in change
      ccontent = changeicon + " " + portfolio + str(round(change * multiplier,2)) + sigma + " (" + str(round(changepc*100, 2)) + "%)"
      wc, hc = draw.textsize(ccontent, font=changefont)
      draw.text((align_right_offset - wc, changepos_y), ccontent, font = changefont, fill = 0)

      # Last trade timestamp
      try:
        hrs_delta = stock_config.get("stock")[stockname]["hrs_delta"]
      except (KeyError, TypeError):
        pass
      sourcetime = datetime.strptime(S[market + 'MarketTime'], '%Y-%m-%d %H:%M:%S %Z%z')
      tcontent = str((sourcetime + timedelta(hours = hrs_delta)).strftime("%d.%m.%Y %H:%M:%S"))
      wt, ht = draw.textsize(tcontent, font=lasttradefont)
      draw.text((align_right_offset - wt, lasttradepos_y), tcontent, font = lasttradefont, fill = 0)

      # Quantity indicator if given
      if multiplier > 1:
        draw.text((qtypos_x, qtypos_x), "QTY", font = qtyfont, fill = 0)
        draw.text((qtynumpos_x, qtynumpos_y), str(multiplier), font = qtynumfont, fill = 0)
      
      epd.display(epd.getbuffer(Himage))

      # Shutdown handling
      try:
        if poweroff == True:
          logging.info("Shutting down OS.")
          os.system("poweroff")
      except IndexError:
        pass

      # Use stock individual sleep time if available
      try:
        sleeptime = stock_config.get("stock")[stockname]["refresh"]
      except (KeyError, TypeError):
        pass

      logging.info("Sleeping " + str(sleeptime) + "s for " + stockname)
      
      while sleeptime != 0:
        time.sleep(1)
        sleeptime -= 1

        # Button 1, skip to next stock
        if (getbuttonstatus(q, 0)):
          setbuttonstatus(q, 0, False)
          sleeptime = 0

        # Button 4, poweroff
        if (getbuttonstatus(q, 3)):
          poweroff = True
          sleeptime = 0

        logging.debug("Sleeping: " + str(sleeptime))

  except IOError as e:
      logging.info(e)
      
  except KeyboardInterrupt:    
      logging.info("ctrl + c:")
      epd2in7.epdconfig.module_exit()
      exit()

def display_loop(stocks, sleeptime, hrs_delta, q):
  logging.debug("Running main display loop")
  while True:
    for stockname in stocks:

      draw_stock(stockname, sleeptime, hrs_delta, q)

#######################################################

def handleBtn1():
    global btn4_press
    logging.debug("Button 1 pressed")
    logging.info("Skip to next stock")
    setbuttonstatus(q, 0, True)
    btn4_press = False

def handleBtn2():
    global btn4_press
    logging.debug("Button 2 pressed")
    btn4_press = False

def handleBtn3():
    global btn4_press
    logging.debug("Button 3 pressed")
    btn4_press = False

def handleBtn4():
    global btn4_press
    logging.debug("Button 4 pressed")
    if btn4_press == True:
        logging.debug("Button 4 second press")
        logging.info("Starting shutdown")
        poweroff = True
        setbuttonstatus(q, 3, True)
    else:
        logging.debug("Button 4 first press")
        btn4_press = True

def button_loop(q):
  global btn4_press

  btn1 = Button(5)
  btn2 = Button(6)
  btn3 = Button(13)
  btn4 = Button(19)
  
  btn4_press = False

  btn1.when_pressed = handleBtn1
  btn2.when_pressed = handleBtn2
  btn3.when_pressed = handleBtn3
  btn4.when_pressed = handleBtn4
  
  logging.info("Button handling initialized")
  # Without pause it would exit and not handle buttons ;)
  pause()

def setbuttonstatus(q, button, status):
  try:
    buttonstatus = q.get_nowait()
  except Empty:
    buttonstatus = [False, False, False, False]
  buttonstatus[button] = status
  q.put(buttonstatus)

def getbuttonstatus(q, button):
  try:
    buttonstatus = q.get_nowait()
  except Empty:
    buttonstatus = [False, False, False, False]
  q.put(buttonstatus)
  return(buttonstatus[button])

q = multiprocessing.Queue()
displayprocess = multiprocessing.Process(target=display_loop, args=(stocks, sleeptime, hrs_delta, q,))
buttonprocess = multiprocessing.Process(target=button_loop, args=(q,))

if __name__ == '__main__':
  displayprocess.start()
  buttonprocess.start()
  while True:
    if displayprocess.is_alive() == False:
      logging.error("Display process crashed. See log for details. Restarting.")
      displayprocess = multiprocessing.Process(target=display_loop, args=(stocks, sleeptime, hrs_delta, q,))
      displayprocess.start()
    time.sleep(60)
